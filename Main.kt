// Llista amb totes les paraules posibles per jugar
var llistaParaules = arrayOf("amigo","antes","anulo","antro","animo","añejo","anime","anglo","angel","anexo","apice","apios","apilo","apelo","apego","aludo","aludi","alzos","aloes","almos","altos","ancho","ambos","anudo","astro","asumo","artes","arcos","arbol","apuro","apure","arabe","aquel","acido","achis","abuso","adios","actuo","acuse","abaco","abeto","alamo","ajeno","algun","aliño","album","adule","aereo","adulo","aguzo","agape","agudo","biche","brote","bacan","bongo","bache","brisa","bolsa","broma","badil","bafle","becas","baile","belga","biela","bajio","blusa","bajos","bizco","blues","brazo","bigas","bufon","bujia","batey","breva","bueno","bicho","baldo","bofar","balin","balon","balto","banyo","banzo","bantu","bañil","baños","bardo","borda","baron","batik","bemol","bidon","betun","borda","botar","brujo","bucal","budin","bucle","burdo","cable","cabos","caldo","calor","calvo","campo","canes","caney","canje","canto","cañon","caños","cardo","carey","cargo","carie","caros","casto","catre","cuaje","cauto","cebar","cedro","cejas","ceibo","celar","celda","cenar","censo","cenit","cesto","cetro","chips","choza","chuzo","cidra","cifra","ciego","cinta","clavo","clero","clima","comer","costa","coxis","crudo","cueva","curva","cutis","danes","daños","darse","datil","datos","decai","debut","decir","dejar","delta","denso","dento","dicha","dicho","dicta","diera","dieta","digno","dijes","diosa","disco","divan","doble","dobla","docil","dogma","doler","domar","donar","dones","dopar","dormi","dotar","drago","dreno","ducha","ducto","duelo","dueña","dulce","duros","ebano","ebrio","echar","ecuas","edito","educa","efuso","egida","elfos","elijo","elite","eluda","emiti","emula","encia","envia","epica","epoca","equis","ergui","ergos","eriza","erigi","espia","esqui","estar","estoy","etano","etica","etimo","etnia","evado","evito","exito","facil","facto","fagot","falco","falso","fango","feria","farol","fasto","favor","fecha","feliz","felpa","femur","fenix","feroz","feudo","fibra","fiare","ficha","fideo","fiera","fijar","filas","filme","final","finca","fingi","fines","firma","flash","flaco","fleco","flora","flota","flema","fluia","flujo","fobia","fogon","force","forja","forma","fosil","foton","frase","friso","freno","fresa","fruta","fuego","fuera","funda","fusil","fusta","gaban","gajos","galan","gales","galon","gasto","gemas","gemir","genio","gesta","giros","glase","gleba","grifo","gozar","golpe","gotas","gozar","grabe","grado","grafo","gramo","grano","grato","grave","greca","grial","grito","grima","gripe","grito","gruñe","grumo","grupo","gruta","guayo","gueto","guiar","guiño","gusto","haber","habil","hacer","harto","hazlo","hebra","hedor","helar","helio","henar","heroe","hervi","hiato","hidra","hielo","higos","hilar","himno","hindu","hiper","hojas","hogar","horas","hotel","hueco","hueso","huevo","huida","humor","hurto","ibero","ideal","ideas","igneo","igual","ilesa","iluso","impar","impio","ingle","infla","insto","inter","iones","infra","irias","istmo","italo","izare","izote","jabon","jalon","jaque","jayan","jedar","jefas","jolin","jopar","joven","joyas","judio","juego","juega","jugar","junio","jumar","junta","junco","justo","juzga","kanji","karts","kefir","kilos","koine","kurda","kurdo","labio","labro","labor","lados","laico","lamer","lance","lando","lanzo","lapos","lapso","lapiz","largo","lares","latex","latir","laton","leuda","laxos","lease","legar","legia","lejos","lento","leñar","leños","lepra","levar","liber","libra","libre","libro","licua","liceo","licor","lider","ligar","light","lijar","limon","limas","linda","linea","lista","listo","litro","lucha","lucid","lucro","luego","lugar","lunar","lunes","luxar","luzco","madre","malos","mando","mango","manos","maori","mapeo","marzo","marte","matiz","mecha","media","medir","medio","megas","melon","manso","menso","menos","menta","mutar","merco","metal","metas","metro","micro","minar","miope","mixto","mofar","mohin","molar","molde","mundo","monje","monja","moral","motin","mover","mucho","mueca","muela","mujer","mundo","nabos","nacar","nacer","negro","nadir","nadie","naipe","nardo","nariz","natio","naves","nebli","necio","negar","nevar","nexos","nicho","niega","nuevo","nieto","nieta","niñez","nivel","nobel","noche","nitro","nogal","norma","norte","notar","nubes","nubil","nuble","nuero","nuevo","ñoras","ñaque","ñecas","ñatos","ñango","ñoqui","ñames","ñajos","ñonga","ñocas","ñique","ñipes","ñinga","oblea","obito","obten","octal","ocupa","odiar","odres","oible","ojala","ojear","ojera","omega","omita","ondas","ondea","opera","opera","optar","orden","oreja","orina","orion","oruga","ostia","otras","ovulo","pacto","padre","palco","polea","panel","pardo","pared","parto","parte","paseo","pasmo","pasto","patin","patio","pecar","pecho","pedal","pedir","pegar","penal","penar","penca","penas","peñon","perno","pesar","pesca","pezon","piano","picar","pecas","picor","pinta","pinza","piñon","pique","pisar","pista","pitar","pixel","pizca","pivot","place","plano","plato","plazo","plano","pluma","podar","poder","poeta","polca","polen","polea","poner","posar","poste","prado","prima","primo","prisa","prosa","pudin","puber","pudor","pugna","pujar","pulir","pulso","pulse","punir","punta","punza","purga","puzle","quark","queco","queda","quedo","queja","quema","quemi","quena","quepo","quera","quien","quien","queso","quijo","quila","quipa","quina","quino","quiño","quiza","quite","quito","recio","radio","radon","rahez","raido","rioja","rajen","raigo","rango","rapte","repta","rapto","rasco","rasgo","raspe","ratos","raudo","rayos","razon","roble","recai","recta","recia","regia","regla","rehen","reino","reloj","renal","remos","renta","resta","reuma","reuso","reves","riego","rifle","rimas","rinde","ruina","rival","ritmo","roble","rompe","ronca","rosca","rumba","rumie","saber","sabio","sabor","sacro","sajon","solde","saldo","salir","salio","salme","salmo","salon","salte","selva","salve","saman","salto","salud","salve","salvo","sanco","santo","saque","satin","sauce","sardo","sauco","sazon","secar","secta","secua","sedal","sedar","segar","segun","segur","senda","senil","señal","señor","sepia","septo","serio","sexto","sigla","siglo","simil","sobre","socia","solar","sonar","soñar","suave","sucio","sudar","suelo","suero","sufre","sumar","super","sutil","tacho","tacon","tacos","tahur","taino","tajos","talco","tales","talon","talud","tamiz","tango","tarde","tardo","tasco","taxon","tauro","techo","tecla","tedio","tejas","telar","telon","temas","temor","tempo","tendi","tenis","tenor","tenso","terco","temor","termo","terso","tirso","tieso","tilde","tilin","timar","timon","teñir","tiple","tiron","tocar","tomar","tunel","topar","toque","torax","toser","trans","trapo","trial","tribu","trina","tripa","triza","tropa","trova","truco","tumba","tupir","turbo","turno","ubres","ubico","ubica","ulema","ultra","umbra","unido","uncir","uncia","ungir","untar","untos","Urano","urdia","urbes","urgio","urjas","urico","urica","usado","usare","usted","utero","uvula","vacio","vagon","video","valer","valor","vamos","vapor","vanos","vario","varon","vasco","vater","vatio","vease","vedar","velar","velon","vengo","venia","venta","venus","veras","venia","verbo","verso","vermu","verti","viaje","vibra","viejo","vieja","vigia","vigor","viral","visco","vital","viudo","visto","viste","vocal","vodka","volar","votar","vuelo","xecas","xinca","xiote","xolas","yacer","yates","yelmo","yergo","yendo","yerba","yedra","yerma","yerna","yerno","yesca","yogui","yogur","yumbo","yunta","yezgo","zafio","zanco","zenda","zetas","zocas","zombi","zueco","zumba","zumbo","zupia","zuñir","zurci","zurdo","zurda","Jacob","Susan","oscar","David","James","Josue","Javier","Aime","Alexa","Anais","Lucas","Adamo","Agnus","Zoila","angel","Antia","Berta","Vilma","Bimba","Celia","Cesar","Nubia","Lucia","Sofia","Lucas","Dario","Mario","Mauro","Frank","Dimas","Edipo","Nicol","Jolie","Julio","Ester","Guido","Linda","Lucio","Marco","Pablo","Ramon","Cadiz","Ibiza","Argel","Cuzco","Andes","Alpes","Sudan","China","Ejion","Judea","Kenia","Moscu","Petra","Nepal","Quito","Saudi","Vegas","Siena","Suiza","ñandu","oveja","cebra","hiena","yegua","cerdo","bagre","lemur","cobra","coqui","okapi","leona","tigre","lince","saino","erizo","ganso","piton","ostra","tucan","mirlo","morsa","potra","corua","mosca","cebra","coati","tenia","chivo")

/**
 * @author Adrián Sánchez Rodríguez
 * @version Versió 1, Data: 08/01/2023
 * Aquesta funció explica el funcionament del joc i el significat de cada color
 */
fun menuBenvinguda() {
    println()
    println("BENVINGUT A WORDLE!!")
    println("Aquest joc tracta de endevinar una paraula oculta de 5 lletres")
    println("Quan introduiexis una paraula pintarà cada lletra d'un color depenent de si esta a la posició correcta, en una altra posició de la paraula o directament no hi és.")
    println("Si la pinta de color verd la lletra està a la posició correcta, si la pinta en groc la lletra està a una altra posició de la paraula i si la deixa en gris és que aquella letra no està a la paraula")
    println()
}


/**
 * @author Adrián Sánchez Rodríguez
 * @version Versió 1, Data: 08/01/2023
 * @param paraula Paraula a endevinar
 * @param paraulaUsuari Paraula que ha introduït l'usuari
 * Aquesta funció pinta de colors les lletres de la paraula de l'usuari depenent de si la lletra està a la posició correcta, a una altra posició o directament no està.
 */
fun comptarLletresIPintarles(paraula: String, paraulaUsuari: String) {
    var posicio:Int
    val comptadorLletres= mutableMapOf<Char, Int>()
    for (i in paraula){
        comptadorLletres.putIfAbsent(i, 0)
        comptadorLletres[i] = comptadorLletres[i]!! + 1
    }
    posicio = 0

    // Comproba si una lletra de la paraula que ha introduit l'usuari està a la paraula correcta i depenent de si està a la posició correcta, en una altra posició o directament no està a la paraula correcta la pintarà verda, groga o la deixarà amb el fons gris
    for (i in paraulaUsuari){
        if (paraula[posicio] == i) {
            print("\u001b[42m$i\u001b[0m")
            comptadorLletres[i] = comptadorLletres[i]!! - 1

        }else if (comptadorLletres[i]!=null && comptadorLletres[i]!!.toInt()>0){

            print("\u001b[43m$i\u001b[0m")
            comptadorLletres[i] = comptadorLletres[i]!! - 1

        }else{
            print(i)
        }
        posicio++
    }
    println(" ")
}


/**
 * @author Adrián Sánchez Rodríguez
 * @version Versió 1, Data: 08/01/2023
 * Li pregunta a l'usuari si vol seguir jugant i li indica les dues opcions posibles que pot triar
 */
fun missatgeSeguirJugant() {
    println("¿Quieres seguir jugando? Introduce un 0 para seguir jugando o un 1 para salir")
}

/**
 * @author Adrián Sánchez Rodríguez
 * @version Versió 1, Data 08/01/2023
 * Codi principal del joc
 */
fun main(args: Array<String>) {
    // Inicialització de les variables
    var paraula = llistaParaules.random()
    var intents = 6
    var seguirJugant = 1
    //blanco = \u001b[47m
    //verde = \u001b[42m
    //amarillo = \u001b[43m
    //reset = \u001b[0m
    var posicio:Int
    var paraulaUsuari:String = " "

    menuBenvinguda()

    // Inici del do-while
    do {
        println("Intentos restantes: $intents")
        paraulaUsuari= readLine()!!.toString()
        // En cas de que la paraula introduida no sigui de 5 lletres la torna a demanar fins a que aquesta sigui de 5 lletres
        while (paraulaUsuari.length!=5){
            println("La palabra tiene que ser de 5 letras:")
            paraulaUsuari= readln().toString()
        }

        comptarLletresIPintarles(paraula, paraulaUsuari)
        intents--

        if(intents==0){
            println("Ohhhh, la palabra correcta era $paraula")
            missatgeSeguirJugant()
            seguirJugant = readln().toInt()
            println()
            if (seguirJugant==0) {
                intents=6
                paraula = llistaParaules.random()
            }
        }else if (paraulaUsuari==paraula){
            println("Enhorabuena, has acertado la palabra!!!")
            missatgeSeguirJugant()
            seguirJugant = readln().toInt()
            println()
            if (seguirJugant==0) {
                intents=6
                paraula = llistaParaules.random()
            }
        }
    }while (intents>0 && paraulaUsuari!=paraula || seguirJugant != 1)
}
